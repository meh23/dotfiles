#!/bin/bash

set -euo pipefail

readonly regexes=(
  '.*\.(sh|sh\.tmpl)'
  '.*/dot_(bash|profile).*'
  '.*/direnvrc'
)

for re in "${regexes[@]}"; do
  echo "finding files to check with $re"
  find . -type f -regextype posix-extended -regex "$re" | while read -r f; do
    echo "checking $f"
    shellcheck --severity=info --rcfile=.shellcheckrc --color=always "$f"
  done
done
