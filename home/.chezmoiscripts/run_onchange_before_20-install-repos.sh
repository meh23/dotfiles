#!/bin/bash

readonly repo_rpms=(
  https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
)

sudo yum install -y "${repo_rpms[@]}"
