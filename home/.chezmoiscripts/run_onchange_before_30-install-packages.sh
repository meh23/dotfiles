#!/bin/bash

readonly packages=(
  bash-completion
  java-latest-openjdk
  glibc-langpack-en
  glibc-locale-source
  jq
  podman
  podman-docker
  procps
  python3.12
  unzip
  vim
  xz
  zip
)

sudo yum install -y "${packages[@]}"
