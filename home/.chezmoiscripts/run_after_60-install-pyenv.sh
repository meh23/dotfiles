#!/bin/bash

if ! [ -d "$HOME/.pyenv" ] && ! [ -s "$HOME/.pyenv/bin/pyenv" ]; then
  rm -f "$HOME/.pyenv"
  curl https://pyenv.run | bash
else
  "$HOME/.pyenv/bin/pyenv" update
fi

readonly packages=(
  gcc
  make
  patch
  zlib-devel
  bzip2
  bzip2-devel
  ncurses-devel
  # readline-devel
  sqlite
  sqlite-devel
  openssl-devel
  # tk-devel
  libffi-devel
  xz-devel
)

sudo yum install -y "${packages[@]}"
