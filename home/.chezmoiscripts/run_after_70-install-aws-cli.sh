#!/bin/bash

if [ -x "${HOME}/aws/install" ]; then
  if [ -d /usr/local/aws-cli/v2/current ]; then
    sudo "${HOME}/aws/install" --update
  else
    sudo "${HOME}/aws/install"
  fi
fi
